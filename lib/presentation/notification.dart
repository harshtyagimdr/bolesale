import 'package:flutter/material.dart';

import './custom/switch_button.dart';
import '../utils/string_value.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List notification_title = [
    StringValue.ACCOUNT,
    StringValue.SHIPMENT,
    StringValue.RECOMMENDATION,
    StringValue.SHOPPING_NOTIFICATION,
    StringValue.DEALS,
    StringValue.COMMUNITY
  ];
  List notification_subtitle = [
    StringValue.SUB_ACCOUNT,
    StringValue.SUB_SHIPMENT,
    StringValue.SUB_RECOMMENDATION,
    StringValue.SUB_SHOPPING_NOTIFICATION,
    StringValue.SUB_DEALS,
    StringValue.SUB_COMMUNITY
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
        //   Navigator.of(context).pop(MaterialPageRoute);
        // }),
        title: Text("Notifications"),
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Notifications",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "Receive push notifications on your deviceabout your packages",
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 14.0,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: SingleChildScrollView(
                  child: ListView.builder(
                      padding: EdgeInsets.all(5.0),
                      shrinkWrap: true,
                      itemCount: notification_title.length,
                      itemBuilder: (context, index) {
                        return Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                notification_title[index],
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              subtitle: Text(notification_subtitle[index],
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12.0)),
                              trailing: SwitchButton(),
                            ),
                            Divider(
                              height: 2.0,
                            ),
                          ],
                        );
                      }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
