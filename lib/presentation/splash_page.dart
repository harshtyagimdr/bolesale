import 'dart:async';
import 'package:bolesale/utils/globals.dart';
import "package:bolesale/model/user.dart";
import "package:bolesale/presentation/custom/image_container.dart";
import "package:bolesale/presentation/custom/ph_scaffold.dart";
import "package:bolesale/presentation/login_page.dart";
import "package:bolesale/utils/styles.dart";
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:bolesale/presentation/home/home_page.dart';

import 'login_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  double defaultHeight = 812;
  double defaultWidth = 375;
  Timer timer;
   User loggedInUser;

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(
        width: defaultWidth, height: defaultHeight, allowFontScaling: false)
      ..init(context);
    _timer(context);

    return PHScaffold(
      body: Center(
        child: ImageContainer(
          height: 250,
          width: 300,
          image: AssetImage(
            Styles.APP_LOGO,
          ),
        ),
      ),
    );
  }

  _timer(context) {
    timer = Timer(Duration(seconds: 5), () {
      checkUser(context);
      timer.cancel();
    });
  }
  checkUser(context) async {
    User loggedInUser = await preferencesService.getAuthUser();
    if (loggedInUser != null) {
      _navigateToHomePage(context);
    } else {
      _navigateToLoginPage(context);
    }
  }

  _navigateToLoginPage(context) {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
  }
  _navigateToHomePage(context){
     Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Scaffold(body: Home())));
  }
}
