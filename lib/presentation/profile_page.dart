import 'package:flutter/material.dart';
import 'custom/circle_avatar_container.dart';
import 'custom/ph_scaffold.dart';
// import '../utils/styles.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PHScaffold(
      appBar: AppBar(
        leading: IconButton(
          icon:Icon(Icons.arrow_back), 
          onPressed: (){}),
          title: Center(
            child: Text("Profile Page"),
          ),
      ),
      body: Column(
        children: <Widget>[
          CircleAvatarContainer(
            image: Image.asset('assets/user.png'),
            height: 10.0,
            // showEditIcon: true,
          ),
          // ListView.builder(
          //   itemBuilder: (ctx)=>{

          //   })
        ],
      ),
    );
  }
}