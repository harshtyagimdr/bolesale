import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import "package:bolesale/model/user.dart";
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:bolesale/utils/styles.dart';
import 'package:bolesale/presentation/custom/image_container.dart';
import 'package:bolesale/presentation/custom/ph_scaffold.dart';
import 'package:bolesale/services/google_service.dart';
import 'package:bolesale/utils/globals.dart';
import 'package:bolesale/utils/string_value.dart';
import "package:bolesale/presentation/home/home_page.dart";

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoading = false;
  DocumentReference documentReference;

  @override
  Widget build(BuildContext context) {
    return PHScaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Styles.PRIMARY_COLOR),
              ),
            )
          : Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: ScreenUtil.instance.setHeight(100),
                  ),
                  ImageContainer(
                    height: 250,
                    width: 190,
                    image: AssetImage(
                      Styles.APP_LOGO,
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil.instance.setHeight(50),
                  ),
                  SignInButton(
                    Buttons.Google,
                    onPressed: () => _googleSignIn(),
                  ),
                ],
              ),
            ),
    );
  }

  _googleSignIn() async {
    setState(() {
      isLoading = true;
    });
    try {
      final FirebaseUser signedInUser =
          await GoogleService.getInstance().signInWithGoogle();
      Map<String, dynamic> value = {
        'uid': signedInUser.uid,
        'photoUrl': signedInUser.photoUrl,
        'email': signedInUser.email,
        'phoneNumber': signedInUser.phoneNumber,
        'displayName': signedInUser.displayName
      };
      User user = User.fromJson(value);
      print("login done");
      await _addUserDetails(user);
      setState(() {
        isLoading = false;
      });
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Home()));
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      showSnackbar(StringValue.PLEASE_TRY_AGAIN_LATER, context);
      print("error in google login");
      print(e);
    }
  }

  Future<void> _addUserDetails(User user) async {
    documentReference =
        Firestore.instance.document("users_details/${user.uid}");
    try {
      DocumentSnapshot documentSnapshot = await documentReference.get();
      String token = await firebaseMessaging.getToken();
      user.phoneToken = token;
      var data = User.toJson(user);
      if (documentSnapshot.exists) {
        user = User.fromJson(documentSnapshot.data);
        user.phoneToken = token;
        data = User.toJson(user);
        await documentReference.updateData(data);
        print("User already exist");
      } else {
        await documentReference.setData(data);
        print("User added to database");
      }
      await preferencesService.setAuthUser(user);
    } catch (e) {
      print(e);
      throw e;
    }
    return;
  }
}
