import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PHScaffold extends StatelessWidget {
  PHScaffold({
    this.body,
    this.fab,
    this.appBar,
  });

  Widget body;
  Widget appBar;
  Widget fab;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      floatingActionButton: fab,
      body: Container(
        padding: EdgeInsets.only(
            left: ScreenUtil.instance.setWidth(15),
            right: ScreenUtil.instance.setWidth(15),
            top: ScreenUtil.instance.setHeight(20)),
        child: body,
      ),
    );
  }
}
