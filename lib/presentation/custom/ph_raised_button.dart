import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import "package:bolesale/utils/styles.dart";

class PHRaisedButton extends StatelessWidget {
  final Widget child;
  final double width;
  final Color color;
  final VoidCallback onTap;

  PHRaisedButton({
    @required this.child,
    @required this.onTap,
    this.width = 300,
    this.color = Styles.TRANSPARENT,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      key: key,
      child: Container(
        margin: EdgeInsets.symmetric(
          vertical: ScreenUtil.instance.setHeight(30),
        ),
        constraints:
            BoxConstraints(minHeight: ScreenUtil.instance.setWidth(50)),
        width: ScreenUtil.instance.setWidth(width),
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.all(
              Radius.circular(ScreenUtil.instance.setWidth(10)),
            )),
        child: Center(
          child: child,
        ),
      ),
      onTap: () => onTap(),
    );
  }
}
