import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:bolesale/utils/styles.dart';

class CircleAvatarContainer extends StatelessWidget {
  CircleAvatarContainer(
      {@required this.image,
      @required this.height,
      this.showEditIcon = false,
      this.editIconHeight = 60.0,
      this.showBorder = true,
      this.bgColor = Styles.TRANSPARENT,
      this.editFunction,
      this.onTap});

  final Widget image;
  final double height;
  final bool showEditIcon;
  final double editIconHeight;
  final bool showBorder;
  final Color bgColor;
  Function editFunction;
  Function onTap;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        InkWell(
          onTap: onTap ?? () {},
          child: Container(
            height: ScreenUtil.instance.setWidth(height),
            width: ScreenUtil.instance.setWidth(height),
            decoration: BoxDecoration(
              border: showBorder
                  ? Border.all(
                      width: ScreenUtil.instance.setWidth(3),
                      color: Styles.BLUE_COLOR,
                    )
                  : null,
              color: bgColor,
              shape: BoxShape.circle,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(
                ScreenUtil.instance.setWidth(height / 2),
              ),
              child: image,
            ),
          ),
        ),
        showEditIcon
            ? Positioned(
                right: 0,
                bottom: 0,
                child: GestureDetector(
                  child: Container(
                    height: ScreenUtil.instance.setWidth(editIconHeight),
                    width: ScreenUtil.instance.setWidth(editIconHeight),
                    decoration: BoxDecoration(
                      color: Styles.BLUE_COLOR,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.edit,
                        color: Styles.BLUE_COLOR,
                        size: 20,
                      ),
                    ),
                  ),
                  onTap: editFunction,
                ),
              )
            : SizedBox()
      ],
    );
  }
}
