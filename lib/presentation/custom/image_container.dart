import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import "package:bolesale/utils/styles.dart";

class ImageContainer extends StatelessWidget {
  ImageContainer(
      {this.height,
      this.width,
      this.color = Styles.TRANSPARENT,
      this.isImage = true,
      this.isImageRotated = true,
      this.padding = 0,
      this.child,
      this.image,
      this.onTap});

  double height;
  double width;
  bool isImage;
  double padding;
  bool isImageRotated;
  Color color;
  ImageProvider image;
  Function onTap;
  Widget child;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(0),
        padding: EdgeInsets.all(0),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(
            Radius.circular(ScreenUtil.instance.setWidth(10)),
          ),
        ),
        height: ScreenUtil.instance.setHeight(height),
        width: ScreenUtil.instance.setWidth(width),
        child: Stack(
          children: <Widget>[
            isImage
                ? Container(
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.all(padding),
                    alignment: Alignment.center,
                    height: ScreenUtil.instance.setHeight(height),
                    child: Image(
                      image: image,
                      fit: BoxFit.contain,
                    ),
                  )
                : child,
          ],
        ),
      ),
    );
  }
}
