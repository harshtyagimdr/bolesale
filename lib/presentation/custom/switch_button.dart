import 'package:flutter/material.dart';

class SwitchButton extends StatefulWidget {
  @override
  _SwitchButtonState createState() => _SwitchButtonState();
}

class _SwitchButtonState extends State<SwitchButton> {
  bool swichControl=false;
  void toggleSwitch(bool value)
  {
    if(swichControl==false)
  {
    // setState(() {
    //   swichControl=true;
    // });
    showDialog(
      context:context,
      
          child: AlertDialog(
            shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(20.0)),
        title: Text("Do you want to allow notification?"),
        actions: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(onPressed: (){
                setState(() {
        swichControl=true;
      });
      Navigator.of(context).pop();
              }, child: Text("OK")),
              FlatButton(onPressed: (){
                setState(() {
        swichControl=false;
      });
      Navigator.of(context).pop();
              }, child: Text("cancel"))
            ],
          )
          
        ],
      ), 
    );
  }
  else
  {
    // setState(() {
    //   swichControl=false;
    // });
    showDialog(
      context:context,
          child: AlertDialog(
            shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(20.0)),
        title: Text("Do you want to disable notification?"),
        actions: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(onPressed: (){
                setState(() {
        swichControl=false;
      });
      Navigator.of(context).pop();
              }, child: Text("OK")),
              FlatButton(onPressed: (){
                setState(() {
        swichControl=true;
      });
      Navigator.of(context).pop();
              }, child: Text("cancel"))
            ],
          )
          
        ],
      ), 
    );
  }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Transform.scale(
        scale: 1.0,
        child: Switch(
          value: swichControl, 
          onChanged: toggleSwitch,
          activeColor: Colors.purple,
          activeTrackColor: Colors.white,
          inactiveTrackColor: Colors.grey,
          inactiveThumbColor: Colors.white,
        ),
    ),
    );
  }
}