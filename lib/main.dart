import 'package:bolesale/presentation/Payment.dart';
import 'package:bolesale/presentation/account.dart';
import 'package:bolesale/presentation/cart.dart';
import 'package:bolesale/presentation/categorise.dart';
import 'package:bolesale/presentation/checkout.dart';
import 'package:bolesale/presentation/home/home_page.dart';
import 'package:bolesale/presentation/notification.dart';
import 'package:bolesale/presentation/order.dart';
import 'package:bolesale/presentation/product_details.dart';
import 'package:bolesale/presentation/settings.dart';
import 'package:bolesale/presentation/shop/shop.dart';
import 'package:bolesale/presentation/wishlist.dart';
import 'package:bolesale/utils/string_value.dart';
import 'package:bolesale/utils/styles.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: StringValue.APP_TITLE,
      theme: ThemeData(
        primarySwatch: Styles.PRIMARY_COLOR,
      ),
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => Home(),
        '/home': (BuildContext context) => Home(),
        '/shop': (BuildContext context) => Shop(),
        '/categorise': (BuildContext context) => Categorise(),
        '/wishlist': (BuildContext context) => WishList(),
        '/cart': (BuildContext context) => CartList(),
        '/settings': (BuildContext context) => Settings(),
        '/products': (BuildContext context) => Products(),
        '/orders': (BuildContext context) => Oder_History(),
        '/checkout': (BuildContext context) => Checkout(),
        '/payment': (BuildContext context) => Patment(),
        '/account': (BuildContext context) => Account_Screen(),
        '/notification': (BuildContext context) => NotificationPage(),
      },
    );
  }
}
