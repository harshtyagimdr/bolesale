import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends _User with _$User {
  static User fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static Map<String, dynamic> toJson(User user) => _$UserToJson(user);
}

abstract class _User with Store {
  @observable
  String uid;

  @observable
  @JsonKey(name: 'displayName')
  String name;

  @observable
  String email;

  @observable
  @JsonKey(name: 'phoneNumber')
  String phone;

  @observable
  @JsonKey(name:'device_token')
  String phoneToken;

  @computed
  get getAvatarUrl {
    String prefix;
    var imgNames = name.split(" ");
    prefix = imgNames[0].substring(0, 1);

    if (imgNames.length > 1) {
      prefix += imgNames[1].substring(0, 1);
    }

    return 'https://dummyimage.com/300x300/ffc997/ffffff.png&text=' + prefix;
  }
}
