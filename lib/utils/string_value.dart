abstract class StringValue {
  //APP TITLE
  static const String APP_TITLE = "BoleSale";

  static const String LOGIN_WITH_GOOGLE = 'Login with Google';
  static const String LOG_OUT = 'Log Out';

  static const String CONTINUE_AS_GUEST = 'Continue as Guest';
  static const String LOGIN_WITH_PHONE_NO = 'Login with Phone no.';


  static const String PLEASE_TRY_AGAIN_LATER = 'Please try again later';
   static const String SHOW_MORE_OPTION = 'Show More Option !';
  static const String HIDE_MORE_OPTION = 'Hide More Option !';
  static const String WHERE_TO_DELIVER = 'WHERE TO DELIVER ?';
  static const String NEW_ARRIVALS = 'New Arrivals';
  // Notification section
  // title
  static const String  ACCOUNT = "Your Account";
  static const String SHIPMENT = "Your Shipment";
  static const String RECOMMENDATION = "Recommendation For You";
  static const String DEALS ="Your Watched and Waitlisted Deals";
  static const String SHOPPING_NOTIFICATION = "Shopping Notification";
  static const String COMMUNITY ="Your Bolsale Community";
  // subtitle
  static const String  SUB_ACCOUNT = "Get notified for accounnt alerts";
  static const String SUB_SHIPMENT = "Find out when packages ship and arrive";
  static const String SUB_RECOMMENDATION = "Get recommendation and offers tailored for you";
  static const String SUB_DEALS ="Find out when lightening deals happen";
  static const String SUB_SHOPPING_NOTIFICATION = "Get helpful links when you shop with us";
  static const String SUB_COMMUNITY ="Recieve updates from customer Q&A, Reviews, Follows, and more."; 
}
