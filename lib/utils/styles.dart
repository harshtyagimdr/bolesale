import 'package:flutter/material.dart';

abstract class Styles {
  static const Color PRIMARY_COLOR = Colors.purple;
  static const Color SECONDARY_COLOR = Color.fromRGBO(163, 22, 223, 1);
  static const Color WHITE_COLOR = Colors.white;
  static const Color BLACK_COLOR = Colors.black;
  static const Color RED_COLOR = Colors.red;
  static const Color TRANSPARENT = Colors.transparent;
  static const Color BLUE_COLOR = Colors.blue;
  static const Color GREY_COLOR = Colors.grey;
  static const Color GREEN_COLOR = Colors.green;

  // Assets
  static const String APP_LOGO = 'assets/logo.png';
  static const String ORDER_DELIVERED_IMAGE = 'assets/order_delivered.png';
  static const String CHECK_ICON = 'assets/icon_check.png';
  static const String USER = 'assets/user.png';
  static const String BANNER1 = 'assets/banner-1.png';
  static const String BANNER2 = 'assets/banner-2.png';
  static const String DRAWERHEADER = 'assets/drawer-header.jpg';
}
