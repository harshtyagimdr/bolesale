import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:bolesale/services/preference_service.dart';
import 'package:bolesale/utils/styles.dart';

final PreferencesService preferencesService = PreferencesService.getInstance();
final FirebaseAuth auth = FirebaseAuth.instance;
final Firestore firestore = Firestore.instance;
final FirebaseMessaging firebaseMessaging = FirebaseMessaging();

Widget showSnackbar(String text, BuildContext context,
    {String actionText, Function onTap}) {
  if (context != null) {
    Scaffold.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(
            text,
          ),
          backgroundColor: Styles.BLACK_COLOR,
          duration: Duration(milliseconds: 2500),
        ),
      );
  }
}
